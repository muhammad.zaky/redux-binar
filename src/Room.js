import { useSelector, useDispatch } from "react-redux";
import { increment, decrement } from "./redux/actions";
import { useEffect, useState } from "react";

const Room = () => {
    const counter = useSelector(state => state.counter)
    const name = useSelector(state => state.name)
    const dispatch = useDispatch();
    const [nilai, setNilai] = useState(counter);
    
    useEffect(() => {
        console.log('a', counter)
    }, [nilai])
    return (
        <div>
            <button onClick={() => dispatch(decrement(name))}>-</button>
            <span>{counter[name]}</span>
            <button onClick={() => dispatch(increment(name))}>+</button>
        </div>
    )
}

export default Room;