import { useState } from "react";
import "./App.css";
import Detail from "./Detail";
import Reducers from "./redux/reducers";
import { createStore } from "redux";
import { Provider, useDispatch } from "react-redux";
import List from "./List";

function App() {
  const [room, setRoom] = useState("1A");
  const store = createStore(Reducers);

  return (
    <Provider store={store}>
      <Detail room={room}></Detail>
      <List></List>
    </Provider>
  );
}

export default App;
