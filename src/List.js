import { useDispatch, useSelector } from "react-redux";
import { setName } from "./redux/actions";

const List = () => {
  const dispatch = useDispatch();
  const counter = useSelector(state => state.counter);
  return (
    <ul>
      <li onClick={() => dispatch(setName("zaky"))}>Muhammad Zaky {counter.zaky}</li>
      <li onClick={() => dispatch(setName("naufal"))}>Naufal {counter.naufal}</li>
      <li onClick={() => dispatch(setName("albert"))}>albert {counter.albert}</li>
    </ul>
  );
};

export default List;
