import { combineReducers } from "redux";
import NameReducer from "./NameReducer";
import CounterReducer from "./CounterReducer";

const Reducers = combineReducers({
    name: NameReducer,
    counter: CounterReducer
})

export default Reducers;