const initialData = {
    zaky: 0,
    naufal: 0,
    albert: 0
}
const CounterReducer = (state = initialData, action) => {
    console.log(state)
    switch (action.type) {
        case "INCREMENT":
            return { ...state, [action.payload]: state[action.payload] + 1}
        case "DECREMENT":
            return { ...state, [action.payload]: state[action.payload] - 1}
        default:
            return state;
    }
}

export default CounterReducer;