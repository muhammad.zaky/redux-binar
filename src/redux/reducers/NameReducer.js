const NameReducer = (state = "zaky", action) => {
    switch(action.type){
        case "setName":
            return action.payload;
        default:
            return state;
    }
}

export default NameReducer;