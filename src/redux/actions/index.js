export const setName = (name) => {
    return {
        type: "setName",
        payload: name
    }
}

export const increment = (name) => {
    return {
        type: "INCREMENT",
        payload: name
    }
}

export const decrement = (name) => {
    return {
        type: "DECREMENT",
        payload: name
    }
}