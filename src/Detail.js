import Room from "./Room";
import { useSelector } from "react-redux";

const Detail = ({}) => {
    const activeName = useSelector(state => state.name)
    return (
        <div>
            <h1>{activeName}</h1>
            <Room></Room>
        </div>
    )
}

export default Detail;